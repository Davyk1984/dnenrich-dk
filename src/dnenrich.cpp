#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <algorithm>

using namespace std;

#include "char_tok.h"
#include "crandom.h"

#define GENERIC_TYPE "*:*"


struct set_t { 
  set_t( const string & name , const int id ) : name(name) , id(id) { }
  string name;
  int id;

  double statistic;  // test
  double statistic0; // baseline
  
  map<int,double> recur; // recurrence at N
  map<int,double> recur0; // recurrence at N

  map<int,int> denom; // denominator, incase mean needed
  map<int,int> denom0;  // (note, uses [1] for main test)
  
  map<int,vector<string> > recurring; // just for output
  map<int,vector<string> > recurring0; // just for output
  
  set<string> members;
};


struct stats_t
{
  map<string,double> setstat; // primary set statistic
  map<string,map<int,double> > recur; // set-stat thresholding on T
};

struct gene_t {
  gene_t( const string & name , const int id ) : name(name) , id(id) { }
  string name; // actual gene-name
  int id; // numerical ID
  map<set_t*,double> setwgt; // map gene --> set(wgt)
  bool operator<( const gene_t & rhs ) const { return id < rhs.id; }
};


struct target_t {
  // relate target to genes (i.e. as single target can hit >1 gene)
  vector<gene_t*> genes;
};

struct types_t {
	int getTypeIndex( const std::string & type )
	{
		std::map<std::string,int>::const_iterator findTypeIt = type_index.find(type);
		if (findTypeIt != type_index.end())
			return findTypeIt->second;

		int typeInd = type_index.size();
		type_index[ type ] = typeInd;
		type_name.push_back( type );

		return typeInd;
	}

	std::map<std::string,int> type_index;
	std::vector<std::string> type_name;
};
types_t TYPES;
const int GENERIC_TYPE_INDEX = TYPES.getTypeIndex(GENERIC_TYPE);

typedef std::vector<int> GeneLengthsVector;
struct coverage_t { 
	// for a specific trio, have actualized/effective coverage; should always be 'ntargets' length
	// bases 'covered' for that target/variant type at whatever criterion was used to define RD matrix
	std::map<int, GeneLengthsVector> bp; // for each type (row), depth across all targets (col) ( 'trio' specific )

	bool type_exists( int type ) const
	{
		std::map<int, GeneLengthsVector>::const_iterator findTypeCov = bp.find(type);
		return findTypeCov != bp.end() && !(findTypeCov->second.empty());
	}

	bool type_exists( const std::string & t ) const
	{
		int type = TYPES.getTypeIndex(t);
		return type_exists(type);
	}
};
 
struct trio_t {

  trio_t( const string & id ) : id(id) { USE_TRIO = this; }
  std::vector<int>& add_target_row( int type, int n ) { std::vector<int> d(n,0); cov.bp[type] = d; return cov.bp[type]; }
  std::string id;
  //std::string cov_id; // potentially convert non-matching ID to '*' to match wildcard in RD row
  
  vector<vector<gene_t*> > mut; // (i,j) i independent mutations, each i'th hitting j-genes
  vector<double> wgt; // optional weight per mutation
  vector<int> type; // type of mutation
  vector<vector<gene_t*> > origmut; // original muts (for output, not permuted)

  // as above, but if using a baseline set
  vector<vector<gene_t*> > mut0; // (i,j) i independent mutations, each i'th hitting j-genes
  vector<double> wgt0; // optional weight per mutation
  vector<int> type0; // type
  vector<vector<gene_t*> > origmut0; // original muts (for output, not permuted)

  vector<long int> totcov; // type-specific total coverage
  coverage_t cov;

  const trio_t* USE_TRIO;
};



//
// Main (global) data stores
//

unsigned int ntargets;
bool calc_mean;
bool allow_mult_hits_in_same_person;
bool printPermutations;
bool original_data;

map<string,gene_t*> genes;
map<string,set_t*> sets;
vector<target_t> targets;
map<string,trio_t*> trios;
set<string> official_symbols;
map<string,string> aliases;

bool has_background;
set<string> gene_background;

// a single list of mutations, or two (e.g. NS and S)
bool has_baseline_grp;



//
// Helper functions
//



bool background_check( const string & g )
{
  if ( ! has_background ) return true;
  return gene_background.find(g) != gene_background.end();
}

gene_t * addgene( const string & name )
{
  // does this gene already exist? 
  map<string,gene_t*>::iterator ii = genes.find( name );
  if ( ii != genes.end() ) return ii->second;
  gene_t * p = new gene_t( name , genes.size() ); 
  genes[ name ] = p;
  return p;
}

set_t * addset( const string & name )
{
  map<string,set_t*>::iterator ii = sets.find( name );
  if ( ii != sets.end() ) return ii->second;
  set_t * p = new set_t( name , sets.size() ); 
  sets[ name ] = p;
  return p;  
}

trio_t* addtrio( const string & name )
{
  map<string,trio_t*>::iterator ii = trios.find( name );
  if ( ii != trios.end() ) return ii->second;

  trio_t* p = new trio_t( name );
  p->USE_TRIO = p;
  trios[ name ] = p;
  return p;  
}


void calculate_totcov( trio_t * trio ) 
{
	trio->totcov.resize( TYPES.type_index.size() , 0 );

	for (std::map<std::string,int>::const_iterator typeIt = TYPES.type_index.begin(); typeIt != TYPES.type_index.end(); ++typeIt)
	{
		const std::string& typeName = typeIt->first;
		int type = typeIt->second;
		if (trio->cov.type_exists(type)) {
			std::vector<int>& bp = trio->cov.bp[type];
			const unsigned int nt = bp.size();

			// should not happen:
			if ( nt != ntargets ) { cerr << "ERROR: problem with data, discrepant # of targets for trio\n"; exit(1); }

			long int& tc = trio->totcov[type];

			int dropped = 0;
			for ( unsigned int t = 0 ; t < nt ; t++ )
			{
				// if this region has 0 in-background genes, set to zero (i.e. we effectively want to ignore this)
				int actual_ng = 0;
				const int ng = targets[t].genes.size();
				for (int g = 0 ; g < ng ; g++ )
				{
					if ( background_check( targets[t].genes[g]->name ) ) ++actual_ng;
				}

				// are we left with any genes here?
				if ( actual_ng == 0 )
				{
					// if not, set bp length covered to zero:
					bp[t] = 0;
					++dropped;
				}

				// accumulate total coverage for this trio
				tc += bp[t];

			}

			//      std::cout << "tc\t" << trio->cov.type_name[ i ] << "\t" << tc << "\n";

			if ( typeName == GENERIC_TYPE )
				cerr << "__IND\t" << trio->id << "\t" << typeName << "\t"
				<< ntargets - dropped << "\t"
				<< tc << "\n";
		}
	} // next type
}

set<int> trec; // from both test and baseline pooled; we just use this to tell us which levels to look at

stats_t calculate_setstats()
{
  

  // initialise

  stats_t stats;
  
  map<string,set_t*>::iterator ss = sets.begin();
  while ( ss != sets.end() )
    {

      // main statistic
      ss->second->statistic = 0;
      ss->second->statistic0 = 0;      
      
      // recur stats
      ss->second->recur.clear();
      ss->second->recur0.clear();
      
      // denom, (variant count), if mean stats required
      ss->second->denom.clear();
      ss->second->denom0.clear();

      ++ss;
    }

  
  //
  // Get recurrence counts for all genes
  //

  map<gene_t*,int> grec;   // recurrent in test group
  map<gene_t*,int> grec0;  // recurrent in baseline group
  
  map<string,trio_t*>::iterator ii = trios.begin();
  while ( ii != trios.end() )
    {
      vector<vector<gene_t*> >::iterator jj = ii->second->mut.begin();
      while ( jj != ii->second->mut.end() ) 
	{
	  for (unsigned int g=0;g< jj->size(); g++) grec[ (*jj)[g] ]++;
	  ++jj;
	}
      
      // baseline mutations
      if ( has_baseline_grp ) 
	{
	  vector<vector<gene_t*> >::iterator jj = ii->second->mut.begin();
	  while ( jj != ii->second->mut.end() ) 
	    {
	      for (unsigned int g=0;g< jj->size(); g++) grec0[ (*jj)[g] ]++;
	      ++jj;
	    }
	}
      
      ++ii;
    }
  


  //
  // Get recurrence levels
  //
  if (original_data) {  
  map<gene_t*,int>::iterator gg = grec.begin();
  while ( gg != grec.end() ) 
    {
      if ( gg->second > 1 ) trec.insert( gg->second );
      ++gg;
    }
  gg = grec0.begin();
  while ( gg != grec0.end() ) 
    {
      if ( gg->second > 1 ) trec.insert( gg->second );
      ++gg;
    }
  }
  // now trec should contain the recurrence levels to test (for this repl.) and 
  // grec or grec0 contains the appropriate data
  

  //
  // Iterate over each trio/mutation: either calculate sum or mean of variant level stats
  //
  
  ii = trios.begin();

  while ( ii != trios.end() )
    {
      
      
      // mutation counter
      int cnt = 0;
      
      
      //
      // for each mutation
      //

      int jjj = 0;
      vector<vector<gene_t*> >::iterator jj = ii->second->mut.begin();
      while ( jj != ii->second->mut.end() ) 
	{
	  
	  ++jjj;

	  // what genes and therefore sets are impacted by this mutation?
	  set<set_t*> hitsets;
	  for (unsigned int g=0;g< jj->size(); g++)
	    {
	      map<set_t*,double>::iterator kk = ((*jj)[g])->setwgt.begin();
	      while ( kk != ((*jj)[g])->setwgt.end() )
		{
		  hitsets.insert( kk->first );
		  ++kk;
		}
	    }
	  
	  // now, for each of these sets, calculate max of genic statistics

	  set<set_t*>::iterator kk = hitsets.begin();
	  while ( kk != hitsets.end() )
	    {


	      // take max. statistic across genes; these are given by
	      // setwgt (and are set/gene specific)
	      
	      double s = 0;            // main statistic
	      bool found = false;
	      
	      map<int,double> sr; // recurrence-conditioned statistics
	      map<int,bool> foundr;
	      
	      // for all other genes hit by mutation, (if any other)
	      
	      map<int,int> bestg;

	      for (unsigned int g=0;g< jj->size(); g++)
		{
		  gene_t * gene = (*jj)[g];
		  
		  // does this gene belong to hitsets for this mutation?
		  if ( gene->setwgt.find( *kk ) == gene->setwgt.end() ) continue;
		  
		  // main stat
		  if ( ! found ) { found = true; s = gene->setwgt[ *kk ] ; }
		  else if ( gene->setwgt[ *kk ] > s ) s = gene->setwgt[ *kk ] ;
		  
		  // recurrence-conditioned stats
		  set<int>::iterator tt = trec.begin();
		  while ( tt != trec.end() )
		    {
		      if ( grec[ gene ] >= *tt )  // only consider genes meeting recurrence level
			{
			  if ( ! foundr[ *tt ] ) { foundr[ *tt ] = true; bestg[*tt]=g; sr[ *tt ] = gene->setwgt[ *kk ]; }
			  else if ( gene->setwgt[ *kk ] >= sr[ *tt ] ) { bestg[*tt]=g; sr[ *tt ] = gene->setwgt[ *kk ]; }
			}
		      ++tt;
		    }
		  
		}


	      if ( original_data ) 
		{
		  set<int>::iterator tt = trec.begin();
		  while ( tt != trec.end() )
		    {
		      if ( bestg[*tt] != -1 && grec[ (*jj)[bestg[*tt]] ] >= *tt ) 
			(*kk)->recurring[*tt].push_back( (*jj)[bestg[*tt]]->name );
		      ++tt;
		    }
		}
	      
	      
	      //
	      // increment set counter, with genic score weight by variant wgt
	      //
	      
	      (*kk)->statistic += s * ii->second->wgt[ cnt ];
	      if ( calc_mean ) (*kk)->denom[1]++; // track, if mean needed
	      
	      // add in recurrence-conditioned statistics
	      
	      set<int>::iterator tt = trec.begin();
	      while ( tt != trec.end() )
		{
		  (*kk)->recur[ *tt ] += sr[ *tt ] * ii->second->wgt[ cnt ];		  
		  if ( calc_mean && foundr[ *tt ] ) (*kk)->denom[ *tt ]++;
		  ++tt;
		}
	      
	      //
	      // Next set 
	      //
	      
	      ++kk;	      
	      
	    }
	  
	  
	  //
	  // true next mutation
	  //
	  
	  ++cnt;	  
	  ++jj;
	}
      

      //
      // Baseline variants?
      //
      
      if ( has_baseline_grp )
	{

	  int cnt0 = 0;

	  vector<vector<gene_t*> >::iterator jj = ii->second->mut0.begin();
	  while ( jj != ii->second->mut0.end() ) 
	    {
	      
	      // what genes and therefore sets are impacted by this mutation?
	      set<set_t*> hitsets;
	      for (unsigned int g=0;g< jj->size(); g++)
		{
		  map<set_t*,double>::iterator kk = ((*jj)[g])->setwgt.begin();
		  while ( kk != ((*jj)[g])->setwgt.end() )
		    {
		      hitsets.insert( kk->first );
		      ++kk;
		    }
		}

	      // now, for each of these sets, calculate max of genic statistics
	      
	      set<set_t*>::iterator kk = hitsets.begin();
	      while ( kk != hitsets.end() )
		{
		  		  
		  // take max. statistic across genes; these are given by
		  // setwgt (and are set/gene specific)
		  
		  double s = 0;
		  bool found = false;
		  
		  // recurrence-conditioned statistics
		  map<int,double> sr;
		  map<int,bool> foundr;
		  
		  map<int,int> bestg;

		  // for all other genes hit by mutation, (if any other)
		  for (unsigned int g=0;g< jj->size(); g++)
		    {
		      gene_t * gene = (*jj)[g];
		      // does this gene belong to hitsets for this mutation?
		      if ( gene->setwgt.find( *kk) == gene->setwgt.end() ) continue;
		      
		      if ( ! found ) { found = true; s = gene->setwgt[ *kk ] ; }
		      else if ( gene->setwgt[ *kk ] > s ) s = gene->setwgt[ *kk ] ;
		      
		      // recurrence-conditioned stats
		      set<int>::iterator tt = trec.begin();
		      while ( tt != trec.end() )
			{
			  if ( grec0[ gene ] >= *tt )  // only consider genes meeting recurrence level
			    {
			      if ( ! foundr[ *tt ] ) { foundr[ *tt ] = true; bestg[*tt]=g; sr[ *tt ] = gene->setwgt[ *kk ]; }
			      else if ( gene->setwgt[ *kk ] >= sr[ *tt ] ) { bestg[*tt]=g; sr[ *tt ] = gene->setwgt[ *kk ] ; }
			    }
			  ++tt;
			}
		      
		    }
		  
		  
		  if ( original_data ) 
		    {
		      set<int>::iterator tt = trec.begin();
		      while ( tt != trec.end() )
			{
			  if ( bestg[*tt] != -1 && grec[ (*jj)[bestg[*tt]] ] >= *tt ) 
			    (*kk)->recurring0[*tt].push_back( (*jj)[bestg[*tt]]->name );
			  ++tt;
			}
		    }
		  
		  
		  //
		  // increment set counter, with genic score weight by variant wgt
		  //
		  
		  (*kk)->statistic0 += s * ii->second->wgt0[ cnt0 ];
		  if ( calc_mean ) (*kk)->denom0[1]++;
		  
		  // add in recurrence-conditioned statistics
		  
		  set<int>::iterator tt = trec.begin();
		  while ( tt != trec.end() )
		    {
		      (*kk)->recur0[ *tt ] += sr[ *tt ] * ii->second->wgt0[ cnt0 ];
		      if ( calc_mean && foundr[ *tt ] ) (*kk)->denom0[ *tt ]++;
		      ++tt;
		    }
		  
		  
		  //
		  // Next set, for all/any baseline mutations
		  //
		  
		  ++kk;
		  
		} 


	      //
	      // Next independent mutation
	      //

	      ++cnt0;
	      ++jj;
	      
	    }	  
	}
      

      //
      // Next trio 
      //
      
      ++ii;
    } 
  

  //
  // Compile back, and calculate relative score if needed
  //

  map<string,set_t*>::iterator sss = sets.begin();
  while ( sss != sets.end() )
    {

      // use mean rather than (weighted) sum?

      if ( calc_mean ) 
	{
	  if (                     sss->second->denom[1]  > 0 ) sss->second->statistic  /= sss->second->denom[1];
	  if ( has_baseline_grp && sss->second->denom0[1] > 0 ) sss->second->statistic0 /= sss->second->denom0[1];
	}

      // main statistic (possibly baseline-adjusted)
// OLD      stats.setstat[ sss->first ] = sss->second->statistic - ( has_baseline_grp ? sss->second->statistic0 : 0 ) ;
      if ( has_baseline_grp ) 
	{
	  const double d = sss->second->statistic0 + sss->second->statistic ;
	  stats.setstat[ sss->first ] = d > 0 ? sss->second->statistic / d : 0 ;
	}
      else
	stats.setstat[ sss->first ] = sss->second->statistic;

      set<int>::iterator tt = trec.begin();
      while ( tt != trec.end() )
	{
	  // mean vs. sum?
	  if ( calc_mean )
	    {
	      if (                     sss->second->denom[*tt]  > 0 ) sss->second->recur[*tt ]  /= sss->second->denom[*tt];
	      if ( has_baseline_grp && sss->second->denom0[*tt] > 0 ) sss->second->recur0[*tt]  /= sss->second->denom0[*tt];
	    }
	  
	  // (baseline-adjusted) recurrence-conditioned statistic
	  if ( calc_mean || has_baseline_grp || sss->second->recur[ *tt ] > 0 ) 
	    {
	      // CHANGED: from comparative difference to relative proportion, in has_baseline_grp == T case
	      if ( has_baseline_grp )
		{
		  const double d = sss->second->recur0[ *tt ] + sss->second->recur[ *tt ] ; 
		  stats.recur[ sss->first ][ *tt ] = d > 0 ? sss->second->recur[ *tt ] / d : 0 ;
		}
	      else
		stats.recur[ sss->first ][ *tt ] = sss->second->recur[ *tt ];
	      
	    }
	  ++tt;
	}
      ++sss;
    }

  return stats;
}


std::vector<gene_t*> get_random( const int type_index , 
				 const std::vector<long int> & totcov , 
				 const coverage_t & cov )
{
  //cerr << "Randomly placing mutation of type " << TYPES.type_name[type_index] << " (" << totcov[type_index] << " total bases)" << "\n";

  std::vector<gene_t*> r;
  int rnd = CRandom::rand( totcov[type_index] );  
  int cum = -1;
  std::map<int, GeneLengthsVector>::const_iterator findCovIt = cov.bp.find(type_index);
  if (findCovIt == cov.bp.end()) {
	  cerr << "ERROR: cannot find coverage for " << TYPES.type_name[type_index] << "\n";
	  exit(1);
  }
  const std::vector<int>& x = findCovIt->second;
  int nt = x.size();
  for (int t=0;t<nt;t++)
  {
	  cum += x[t];
	  if ( rnd <= cum ) return targets[t].genes;
  }

  cerr << rnd << " " << cum << " " << nt << "\n";
  cerr << "ERROR: failed get_random(), internal error\n"; exit(1);
  
  return r;
}

  

void shuffle_trios()
{
	// this destroys the original data -- just mix up the de novo events
	// for each trio, given their effective coverage

	map<string,trio_t*>::iterator ii = trios.begin();
	while ( ii != trios.end() )
	{
		trio_t* trio = ii->second;

		// original/test mutations
		const int nmut = trio->mut.size();
		for (int j=0;j<nmut;j++)
			trio->mut[j] = get_random( trio->type[j] , trio->USE_TRIO->totcov , trio->USE_TRIO->cov ) ;

		// also independently shuffle any baseline mutations
		if ( has_baseline_grp )
		{
			const int nmut0 = trio->mut0.size();
			for (int j=0;j<nmut0;j++)
				trio->mut0[j] = get_random( trio->type0[j] , trio->USE_TRIO->totcov , trio->USE_TRIO->cov ) ;
		}

		++ii;
	}
}

void print_trios(ostream& stream, string prefix)
{

	map<string,trio_t*>::iterator ii = trios.begin();
	while ( ii != trios.end() )
	{
		trio_t* trio = ii->second;

		// original/test mutations
		stream << prefix << "\t" << "TEST" << "\t" << trio->id;
		const int nmut = trio->mut.size();
		for (int j=0;j<nmut;j++) {
			const vector<gene_t*>& genesHit = trio->mut[j];
			stringstream str;
			for (vector<gene_t*>::const_iterator it = genesHit.begin(); it != genesHit.end(); ++it) {
				if (it != genesHit.begin())
					str << "/";
				const gene_t* g = *it;
				const string& s = g->name;
				str << s;
			}
			stream << "\t" << str.str();
		}
		stream << "\n";

		// baseline mutations
		if ( has_baseline_grp )
		{
			stream << prefix << "\t" << "BASELINE" << "\t" << trio->id;
			const int nmut0 = trio->mut0.size();
			for (int j=0;j<nmut0;j++) {
				const vector<gene_t*>& genesHit0 = trio->mut0[j];
				stringstream str;
				for (vector<gene_t*>::const_iterator it = genesHit0.begin(); it != genesHit0.end(); ++it) {
					if (it != genesHit0.begin())
						str << "/";
					const gene_t* g = *it;
					const string& s = g->name;
					str << s;
				}
				stream << "\t" << str.str();
			}
			stream << "\n";
		}

		++ii;
	}

}


bool check_file_exists( const string & f )
{
  ifstream inp;
  inp.open(f.c_str(), ifstream::in);
  if(inp.fail())
    {
      inp.clear( ios::failbit );
      inp.close();
      cerr << "ERROR: No file [ " << f << " ] exists\n";
      exit(1);
    }
  inp.close();
  return true;
}



int main( int argc , char ** argv )
{

  CRandom::srand( time(0) );


  
  // dnenrich <matrix> <gene.set> <denovo.list>
  
  // 1) matrix format: 'group' 'total_genes', stratified by type (row) {all gene names}...
  
  // 2) gene.set : 3-col, tab-delim;
  //  GENE_ID   SET   WEIGHT
  // output tests all SETs
  
  vector<set_t> s;
  
  // de novo information per trio
  
  if (  argc < 7 )
    {
      cerr << "usage:   ./dnenrich <opt|.> perm-# alias.list depth.matrix  gene.sets  mutation.list {background.list1 background.list2 background.list3 ... [to be intersected]} \n";
      exit(1);
    }
  
  has_background = argc >= 8 ;

  if ( has_background ) 
    cerr << "LOG: running with background gene list\n";
  else
    cerr << "LOG: no background genelist specified\n";
  
  // general options class
  string opt = argv[1];



  // (appropriate for variant/gene weighted scenarios; rather than calculate sum, 
  //  calculate mean)
  calc_mean = opt.find( "m" ) != string::npos;
  allow_mult_hits_in_same_person = opt.find( "x" ) != string::npos;
  printPermutations = opt.find( "p" ) != string::npos;
  

  int nrep = atoi(argv[2]);
  string alias_filename = argv[3];
  string matrix_filename = argv[4];
  string sets_filename  = argv[5];
  string data_filename  = argv[6];

  check_file_exists( alias_filename );
  check_file_exists( matrix_filename );
  check_file_exists( sets_filename );
  check_file_exists( data_filename );

  ifstream ALIAS( alias_filename.c_str() , ios::in );
  ifstream RD( matrix_filename.c_str() , ios::in );
  ifstream SETS( sets_filename.c_str() , ios::in );
  ifstream MUT( data_filename.c_str() , ios::in );


  //
  // Gene aliases
  //
  
  // skip header
  string dummy;
  getline( ALIAS , dummy );
  
  set<string>* multipleAliases = new set<string>();
  int notuniq = 0;
  while ( ! ALIAS.eof() ) {
	  string line;
	  getline( ALIAS , line );
	  if ( line == "" ) continue;
	  int ncol;
	  char_tok tok( line , &ncol , '\t' );
	  if ( ncol < 1 ) continue;

	  string officialSymbol = tok(0);
	  official_symbols.insert(officialSymbol);

	  if ( ncol < 2 ) continue;
	  // assume symbol, followed by many aliases
	  for ( int j=1;j<ncol;j++) {
		  string aliasSymbol = tok(j);
		  if (aliasSymbol == "")
			  continue;

		  // only allow unique alias mappings:
		  set<string>::const_iterator hasMultAliases = multipleAliases->find(aliasSymbol);
		  map<string,string>::iterator findAlias = aliases.find(aliasSymbol);
		  if (hasMultAliases != multipleAliases->end() || (findAlias != aliases.end() && findAlias->second != officialSymbol)) {
			  ++notuniq;
			  //cerr << "NOT UNIQUE: " << aliasSymbol << " -> " << officialSymbol << std::endl;

			  if (findAlias != aliases.end())
				  aliases.erase(findAlias);

			  if (hasMultAliases == multipleAliases->end())
				  multipleAliases->insert(aliasSymbol);
		  }
		  else
			  aliases[aliasSymbol] = officialSymbol;
	  }
  }
  delete multipleAliases;

  ALIAS.close();
  cerr << "LOG: will use " << aliases.size() << " gene-alias mappings\n";
  if ( notuniq ) cerr << "LOG: " << notuniq << " gene-alias mappings were not unique and so skipped\n";

  //
  // Gene background
  //

  if ( has_background ) 
    {
	  const int FIRST_BG_IND = 7;
	  for (int bgInd = FIRST_BG_IND; bgInd < argc; ++bgInd) {
	  string back_filename = argv[bgInd];

      check_file_exists( back_filename );
      ifstream BAK( back_filename.c_str() , ios::in );
      set<string> geneList;
      while ( ! BAK.eof() ) 
	{
	  string g;
	  BAK >> g;
	  if ( g != "" ) {
		  map<string,string>::const_iterator findG = aliases.find(g);
		  if (official_symbols.find(g) == official_symbols.end() && findG != aliases.end()) {
			  cerr << "VERB: matching gene alias " << g << " to " << findG->second << " for background list\n";
			  g = findG->second;
		  }
		  geneList.insert(g);
	  }
	}
      BAK.close();
      if (bgInd == FIRST_BG_IND) {
    	  gene_background = geneList;
      }
      else { // Intersect the background lists:
    	  set<string> intersect;
    	  set_intersection (gene_background.begin(), gene_background.end(),
    			  geneList.begin(), geneList.end(),
    			  inserter(intersect, intersect.begin()));
    	  gene_background = intersect;
      }
    }
      cerr << "LOG: using gene background with " << gene_background.size() << " entries\n";
    }


  //
  // Proc RD matrix
  // 

  // get header, that defines targets and genes

  string hdr;
  getline( RD , hdr );
  if ( hdr == "" ) exit(1);
  int ncol;
  char_tok tok( hdr , &ncol , '\t' );
  
  ntargets = ncol - 2;  // first two cols ignored (GROUP, TYPE), not targets
  
  for (unsigned int i=0;i<ntargets;i++)
    {
      int ng;
      char_tok tok2( tok(i+2) , &ng , '+' );
      target_t t;
      for (int j=0;j<ng;j++) 
	{
	  string g = tok2(j);
	  map<string,string>::const_iterator findG = aliases.find(g);
	  if (official_symbols.find(g) == official_symbols.end() && findG != aliases.end()) {
		  cerr << "VERB: matching gene alias " << g << " to " << findG->second << " for RD matrix\n";
		  g = findG->second;
	  }

	  if ( background_check( g ) )
	    t.genes.push_back( addgene( g ) );	  
	}
      targets.push_back( t );
    }
  
  cerr << "LOG: read " << targets.size() << " targets; " << genes.size() << " unique genes\n";
  if ( ntargets != targets.size() )
    { 
      cerr << "LOG: problem w/ number of targets/parsing header... "
		<< ntargets << " " << targets.size() << "\n"; 
      exit(1); 
    }
  


  //
  // Now get per-trio information
  //

  int rows = 0;

  while ( ! RD.eof() )
    {
      
      string id, type;
      RD >> id >> type;
      
      if ( id == "" ) continue;
      
      trio_t * pt;
      
      if ( trios.find( id ) == trios.end() )
	{
	  pt = new trio_t( id );
	  trios[id] = pt;	  
	}
      else
	pt = trios[id];

      if ( pt->cov.type_exists(type) )
      {
    	  cerr << "WARN: ** read same coverage type/trio combo >1 times: " << id << " " << type << "\n";
    	  exit(1);
      }
      
      int type_index = TYPES.getTypeIndex(type);
      std::vector<int>& row = pt->add_target_row( type_index, ntargets );

      //cerr << "\nadded RD group/type " << type << " for " << id << "\n";

      // read rest of line
      for (unsigned int j=0;j<ntargets;j++) { RD >> row[j]; }
      
      ++rows;

    }
  
  cerr << "LOG: read depth data for " << trios.size() << " trios/groups, for a total of " << rows << " TYPES\n";
  RD.close();

  
  //
  // Read sets
  //
  
  while ( ! SETS.eof() ) 
    {

      // expecting GENE SET WGT 
      string gene, set;
      double w;
      SETS >> gene >> set >> w;
      
      if ( SETS.eof() ) continue;
      
      // gene *must* already exist

      map<string,gene_t*>::iterator ii = genes.find( gene );
      bool found = ii != genes.end();
      
      if ( ! found ) 
	{
	  // can we find via an alias?
	  map<string,string>::const_iterator jj = aliases.find(gene);
 	  if (official_symbols.find(gene) == official_symbols.end() && jj != aliases.end())
	    {
	      found = genes.find( jj->second ) != genes.end();
	      if ( found ) 
		{
		  gene = jj->second; // swap in new gene alias
		  cerr << "VERB: matching gene alias " << jj->first << " to " << jj->second << " for set '" << set << "'\n";
		}
	    }
	}
      
      // skip if could not be found
      if ( ! found ) 
	{
	  cerr << "VERB: **could not find " << gene << " (or any alias of it) in RD matrix, but listed in " << set << "\n";
	  continue;
	}
      

      // get/make set
      set_t * ps = addset( set );
      gene_t * pg = addgene( gene );
      
      // connect up
      ps->members.insert( pg->name );
      pg->setwgt[ ps ] = w;

    }

  SETS.close();
  
  cerr << "LOG: read " << sets.size() << " sets\n";
  

  //
  // Finally, read mutation lists
  //

  set<string> ntrio1;
  set<string> ntrio2;
  set<string> ntrio3;
  
  int nmut = 0 , nmut0 = 0, nmutSkipped = 0;

  has_baseline_grp = false;
  map<string,set<string> > multtracker;

  while ( ! MUT.eof() )
    {
      string trio , gene, type;
      double wgt;
      int grp;
      
      MUT >> trio >> gene >> type >> wgt >> grp;
      if ( trio == "" ) continue;
      if ( gene == "." ) continue;
      
      ntrio1.insert( trio );
      
      // create this trio in any case
      trio_t* pt = addtrio( trio );

      int type_index = TYPES.getTypeIndex(type);

      // type must exist (i.e. the specified row from the RD matrix) for trio pt;
      // otherwise assign this trio to use coverage from the "wild-card" trio ("*"):
      bool found = true;
      if ( ! pt->USE_TRIO->cov.type_exists( type_index ) )
      {
    	  cerr << "WARN: ** Could not find row " << type << " for trio " << trio << " in RD matrix, so will use wild-card ['*'] trio if exists\n";

    	  found = false;
    	  map<string,trio_t*>::const_iterator findWildCardTrio = trios.find( "*" );
    	  if (findWildCardTrio != trios.end()) {
    		  // Before transferring USE_TRIO to be '*', need to check that '*' has all previous types of pt's mutations:
    		  const trio_t* wcTrio = findWildCardTrio->second;
    		  if (pt->USE_TRIO != wcTrio) { // otherwise, the wcTrio is already missing this type as well
    			  bool wcHasAllTypes = true;
    			  for (vector<int>::const_iterator i = pt->type.begin(); i != pt->type.end(); ++i)
    				  if (!wcTrio->cov.type_exists(*i))
    					  wcHasAllTypes = false;
    			  for (vector<int>::const_iterator i = pt->type0.begin(); i != pt->type0.end(); ++i)
    				  if (!wcTrio->cov.type_exists(*i))
    					  wcHasAllTypes = false;
    			  if (!wcTrio->cov.type_exists(type_index))
    				  wcHasAllTypes = false;

    			  if (wcHasAllTypes) {
    				  found = true;
    				  pt->USE_TRIO = wcTrio;
    			  }
    		  }
    	  }
      }

      if (!found) {
    	  if (pt->USE_TRIO->cov.type_exists( GENERIC_TYPE_INDEX )) {
        	  cerr << "WARN: ** Could not find row " << type << " for " << trio << " (or transfer to use depth from wild-card ['*'] trio), so using generic type ['" << GENERIC_TYPE << "'] from trio " << pt->USE_TRIO->id << "\n";
        	  type_index = GENERIC_TYPE_INDEX;
    		  found = true;
    	  }
      }

      if (!found) {
    	  ++nmutSkipped;
    	  cerr << "WARN: ** Could not find row " << type << " for " << trio << " (or transfer to use depth from wild-card ['*'] trio)\n";
    	  continue;
      }

      ntrio2.insert( trio );
      
      int ng = 0;
      char_tok tok2( gene , &ng , ',' );
      
      vector<gene_t*> x;
      for (int j=0;j<ng;j++)
	{
	  string g = tok2(j);
	  map<string,string>::const_iterator findG = aliases.find(g);
	  if (official_symbols.find(g) == official_symbols.end() && findG != aliases.end()) {
		  cerr << "VERB: matching gene alias " << g << " to " << findG->second << " for mutation list\n";
		  g = findG->second;
	  }

	  // gene must exist
	  if ( genes.find( g ) == genes.end() ) continue;
	  if ( ! background_check( g ) ) continue; 

	  if ( ! allow_mult_hits_in_same_person ) 
	    {
	      if ( multtracker[ trio ].find( g ) != multtracker[trio].end() ) 
		{
		  cerr << "WARN: ** not adding multiple hits for " << trio << " / " << g << "\n";
		  continue;
		}
	      multtracker[trio].insert( g );
	    }
	  
	  x.push_back( addgene( g ) );	  

	}
      
      // if at least one trio has a non '1' value here, then assume we'll
      // have two groups in the dataset as a whole, and so the statistics
      // calculated should be relative from grp '1' to grp '0'

      if ( x.size() > 0 && grp == 0 ) has_baseline_grp = true;
      
      //
      // add actual mutation for this trio
      //
      
      if ( x.size() > 0 ) 
	{
	  
	  ntrio3.insert( trio );
	  
	  if ( grp == 1 ) 
	    {
	      ++nmut;	  
	      pt->mut.push_back( x );
	      pt->origmut.push_back( x ); // just for output, i.e. stays unpermuted	  
	      pt->type.push_back( type_index );
	      pt->wgt.push_back( wgt );	  
	    }
	  else if ( grp == 0 ) 
	    {
	      ++nmut0;	  
	      pt->mut0.push_back( x );
	      pt->origmut0.push_back( x ); // just for output, i.e. stays unpermuted	  
	      pt->type0.push_back( type_index );
	      pt->wgt0.push_back( wgt );	  
	    }
	  else 
	    cerr << "WARN: **warning, group code not either '1' or '0', skipping mutation\n";
	}
      
    } // next trio/event pair
  
  MUT.close();
  
  cerr << "LOG: Read mutations from "
       << ntrio1.size() << " trios, of which "
       << ntrio2.size() << " were in RD matrix, of which " 
       << ntrio3.size() << " had one or more de novo\n";
  
  cerr << "LOG: Skipped " << nmutSkipped << " mutations in entire sample\n";
  cerr << "LOG: " << nmut << " test mutations, " << nmut0 << " comparison/baseline mutations, remaining in entire sample\n";
  
  //
  // Calculate total coverage (per type) (given background gene list) for each sample
  //
  
  map<string,trio_t*>::iterator ii = trios.begin();
  while ( ii != trios.end() ) 
    {
      calculate_totcov( ii->second );
      ++ii;
    }


  //
  // Calculate statistics under the original data; one for each set
  //

  original_data = true;

  stats_t original = calculate_setstats();
  
  
  //
  // Empirical p-value for each set
  //

  map<string,int> rep; // main stat
  map<string,double> mean_perm;

  map<string,map<int,int> > repr; // recurrence tests
  map<string,map<int,double> > mean_perm_r;
  
  original_data = false;

  const double EPS = 1e-12;

  if (printPermutations)
	  print_trios(cout, "ORIG\t0");

  for (int R = 0 ; R < nrep ; R++) 
    {
      
      if ( true || R % 10 == 0 ) cerr << ".";
      
      if ( R % 100 == 99 ) cerr << " (" << R+1 << " perms)\n";
      
      //
      // Shuffle 
      //
      
      shuffle_trios();
      
      if (printPermutations) {
    	  stringstream str;
    	  str << "PERM\t" << (R+1);
    	  print_trios(cout, str.str());
      }
      
      //
      // Recount
      //
      
      stats_t permed = calculate_setstats();
      
      map<string,double>::iterator ii = permed.setstat.begin();
      
      while ( ii != permed.setstat.end() )
	{	  
	  
	  // main measure
	  if ( ii->second > ( original.setstat[ ii->first ] - EPS ) ) rep[ ii->first ]++;
	  mean_perm[ ii->first ] += ii->second; 
	  
	  // recurrence tests (i.e. only include genes w/ at least T hits)
	  
	  map<string,map<int,double> >::iterator iii = permed.recur.find( ii->first );
	  if ( iii != permed.recur.end() )
	    {	  
	      map<int,double>::iterator tt = original.recur[ ii->first ].begin();
	      while ( tt != original.recur[ ii->first ].end() )
		{
		  if ( (iii->second)[ tt->first ] >= original.recur[ ii->first ][ tt->first ] ) repr[ ii->first ][ tt->first ]++;
		  mean_perm_r[ ii->first ][ tt->first ] += (iii->second)[ tt->first ];
		  ++tt;
		}
	    }
	  
	  ++ii;
        }
      
    }
  



//
// Report p-values
//


  map<string,double>::iterator kk = original.setstat.begin();
  while ( kk != original.setstat.end() )
    {
      
      double pvalue = ( rep[ kk->first ] + 1.0 ) / (double)( nrep + 1.0 ) ;
      
      stringstream nmut0Str;
      if (nmut0 > 0) nmut0Str << "/" << nmut0;

      cout << kk->first << "\t"
		<< "*\t"
		<< pvalue << "\t"	
		<< kk->second << "\t"
		<< mean_perm[ kk->first ] / (double)nrep << "\t"
		<< sets[ kk->first ]->members.size() << "\t"
		<< nmut << nmut0Str.str() << "\t"
		<< "." 
		<< "\n";      
      
      if ( pvalue <= 0.05 || true )
	{
	  
	  // Iterate over each trio/mutation
	  
	  map<string,trio_t*>::iterator ii = trios.begin();
	  
	  while ( ii != trios.end() )
	    {
	      
	      // wgt counter
	      int cnt = 0;
	      
	      // for each mutation
	      int mut = 0 , mut2 = 0;
	      
	      vector<vector<gene_t*> >::iterator jj = ii->second->origmut.begin();
	      while ( jj != ii->second->origmut.end() ) 
		{
		  
		  mut2 = 0;
		  
		  // for each gene hit by mutation
		  for (unsigned int g=0; g< jj->size(); g++)
		    {		      
		      if ( sets[ kk->first ]->members.find( ((*jj)[g])->name ) != sets[ kk->first ]->members.end() )
			{			  
			  
			  cout << "__GRP1\t"
				    << kk->first << "\t"
				    << ii->first << "\t"
				    << ii->second->wgt[ cnt  ] << "\t"				    
				    << ++mut << "\t"
				    << ++mut2 << "\t"			    
				    << ii->second->origmut.size() << "\t"
				    << ((*jj)[g])->name << "\t"
				    << ((*jj)[g])->setwgt[ sets[ kk->first ] ] << "\n";
			}
		    }
		  
		  ++cnt;
		  ++jj;
		} // next mutation
	      
	      
	      //
	      // now for any baseline mutations
	      //

	      int cnt0 = 0;
	      jj = ii->second->origmut0.begin();
	      while ( jj != ii->second->origmut0.end() ) 
		{
		  
		  mut2 = 0;
		  
		  // for each gene hit by mutation
		  for (unsigned int g=0; g< jj->size(); g++)
		    {		      
		      if ( sets[ kk->first ]->members.find( ((*jj)[g])->name ) != sets[ kk->first ]->members.end() )
			{			  
			  
			  cout << "__GRP0\t"
				    << kk->first << "\t"
				    << ii->first << "\t"
				    << ii->second->wgt0[ cnt0  ] << "\t"				    
				    << ++mut << "\t"
				    << ++mut2 << "\t"			    
				    << ii->second->origmut0.size() << "\t"
				    << ((*jj)[g])->name << "\t"
				    << ((*jj)[g])->setwgt[ sets[ kk->first ] ] << "\n";
			}
		    }
		  
		  ++cnt0;
		  ++jj;
		} // next mutation
	      

	     
	      ++ii;
	    } // next trio

	} // end of optional gene-level output
      
      
      //
      // Output for recurrence-conditioned p-values
      //
      
      map<int,double>::iterator tt = original.recur[ kk->first ].begin();
      while ( tt != original.recur[ kk->first ].end() )
	{
	  
	  double pvalue = ( repr[ kk->first ][ tt->first ] + 1.0 ) / (double)( nrep + 1.0 ) ;
	  
	  cout << kk->first << "\t"
		    << tt->first << "\t"
		    << pvalue << "\t"
		    << tt->second << "\t"
		    << mean_perm_r[ kk->first ][ tt->first ] / (double)nrep  << "\t"
		    << sets[ kk->first ]->members.size() << "\t"
		    << nmut << nmut0Str.str();
	  
	  if ( pvalue < 0.05 || true ) 
	    {
	      cout << "\t";
	      vector<string> & x = sets[ kk->first ]->recurring[ tt->first ];
	      vector<string>::iterator xi = x.begin();
	      while ( xi != x.end() ) { cout << *xi << "(1); "; ++xi; }
	      
	      x = sets[ kk->first ]->recurring0[ tt->first ];
	      xi = x.begin();
	      while ( xi != x.end() ) { cout << *xi << "(0); "; ++xi; }
	    }
	  else
	    cout << "\t.";
	  
	  cout << "\n";
	  
	  ++tt;
	}

      
      //
      // Next set
      //

      ++kk;
    }

  exit(0);
}
